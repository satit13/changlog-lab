package main

import "fmt"

func main() {
	fmt.Println("Hello world")
	fmt.Println("test release npm standard-version command")
	got := add(10, 15)
	fmt.Println(got)
	return
}

func add(x, y int64) (result int64) {
	result = x + y
	return result
}

func multiply(x, y int64) (result int64) {
	result = x * y
	return result
}

func divide(x, y int64) (result int64) {
	result = x / y
	return result
}
