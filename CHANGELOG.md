# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [4.0.1](https://gitlab.com/satit13/changlog-lab/compare/v4.0.0...v4.0.1) (2022-07-08)

## [4.0.0](https://gitlab.com/satit13/changlog-lab/compare/v3.2.0...v4.0.0) (2022-07-08)

## [3.2.0](https://gitlab.com/satit13/changlog-lab/compare/v3.1.2...v3.2.0) (2022-07-08)

### [3.1.2](https://gitlab.com/satit13/changlog-lab/compare/v3.1.1...v3.1.2) (2022-07-08)

### [3.1.1](https://gitlab.com/satit13/changlog-lab/compare/v3.1.0...v3.1.1) (2022-07-08)

## [3.1.0](https://gitlab.com/satit13/changlog-lab/compare/v3.0.1...v3.1.0) (2022-07-07)


### Features

* add divide function ([f6f418a](https://gitlab.com/satit13/changlog-lab/commit/f6f418af3edbd8f4f3d9e8a47f2be8067cb73bff))

### [3.0.1](https://gitlab.com/satit13/changlog-lab/compare/v3.0.0...v3.0.1) (2022-07-07)

## [3.0.0](https://gitlab.com/satit13/changlog-lab/compare/v2.2.1...v3.0.0) (2022-07-07)

### [2.2.1](https://gitlab.com/satit13/changlog-lab/compare/v2.2.0...v2.2.1) (2022-07-07)

## [2.2.0](https://gitlab.com/satit13/changlog-lab/compare/v2.1.0...v2.2.0) (2022-07-07)

## [2.1.0](https://gitlab.com/satit13/changlog-lab/compare/v2.0.1...v2.1.0) (2022-07-07)

### [2.0.1](https://gitlab.com/satit13/changlog-lab/compare/v2.0.0...v2.0.1) (2022-07-07)

## [2.0.0](https://gitlab.com/satit13/changlog-lab/compare/v1.6.0...v2.0.0) (2022-07-07)

## [1.6.0](https://gitlab.com/satit13/changlog-lab/compare/v1.5.3...v1.6.0) (2022-07-07)

### [1.5.3](https://gitlab.com/satit13/changlog-lab/compare/v1.5.2...v1.5.3) (2022-07-07)

### [1.5.2](https://gitlab.com/satit13/changlog-lab/compare/v1.5.1...v1.5.2) (2022-07-07)

### [1.5.1](https://gitlab.com/satit13/changlog-lab/compare/v1.5.0...v1.5.1) (2022-07-07)

## [1.5.0](https://gitlab.com/satit13/changlog-lab/compare/v1.4.0...v1.5.0) (2022-07-07)


### Features

* add multiply function ([9e64d4e](https://gitlab.com/satit13/changlog-lab/commit/9e64d4e349ce168ffe58c4b5bdb3e7928ddcf691))

## [1.4.0](https://gitlab.com/satit13/changlog-lab/compare/v1.3.0...v1.4.0) (2022-07-07)


### Features

* try to generate changelog auto ([26184a3](https://gitlab.com/satit13/changlog-lab/commit/26184a33525df18b859aa185466703717fc958d1))

## [1.3.0](https://gitlab.com/satit13/changlog-lab/compare/v1.2.1...v1.3.0) (2022-07-07)

### [1.2.1](https://gitlab.com/satit13/changlog-lab/compare/v1.2.0...v1.2.1) (2022-07-07)

## [1.2.0](https://gitlab.com/satit13/changlog-lab/compare/v1.1.0...v1.2.0) (2022-07-07)

## [1.1.0](https://gitlab.com/satit13/changlog-lab/compare/v1.0.0...v1.1.0) (2022-07-07)

## 1.0.0 (2022-07-07)


### Bug Fixes

* prevent racing of requests ([e720b28](https://gitlab.com/satit13/changlog-lab/commit/e720b2810b6870117a0a0dac33ef6b578325abfb)), closes [#123](https://gitlab.com/satit13/changlog-lab/issues/123)
