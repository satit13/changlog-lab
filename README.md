# changelog lab for training 

## commit message ตาม concept convention-commit
```
git commit -a -m”<type>[optional scope]: <description>”

ประเภท หรือ type ของ commit มีหลายอย่างตั้งแต่

feat — เป็นการบอกว่ามี feature ใหม่ใน commit นี้นะ (คล้ายๆกับ MINOR ของ semantic versioning)
fix — แก้บัคนั่นแหละ (ประมาณ PATCH )
BREAKING CHANGE — คือการเปลี่ยนแปลงครั้งใหญ่นั่นเอง สามารถใช้คู่กับ feature ก็ได้เช่น BREAKING CHANGE: feat: <description> (เทียบประมาณกับ MAJOR)
doc — แก้ไขไฟล์เอกสาร เช่น README
refactor — การแก้ไขโค้ดเพิ่มให้อ่านง่ายขึ้น หรือเขียนให้ได้ผลเหมือนเดิมแต่วิธีการที่เรียบง่ายขึ้น
```

### ติดตั้ง standard-version 

```
npm install --save-dev standard-version
```
create package.json file with 
```
{
    "scripts": {
        "release": "standard-version"
    },
    "version": "1.0.0",
    "name":"chanlog-lab"
}
```
---
## First version 1.0.0
```
npm run release -- --first-release 
หรือ 
npm run release -- --release-as 1.0.0
```

## push current tag version to remote 
```
git push --follow-tags origin main
```
## Patch running 
``` 
npm run release
```
## Minor running
```
npm run release -- --release-as minor
```
## Major running 
```
npm run release -- --release-as major
```


## reference 
   [Convention Commit](https://www.conventionalcommits.org/en/v1.0.0/).

   [Standard-Version](https://github.com/conventional-changelog/standard-version)

   [อ้างอิงภาษาไทย](https://medium.com/convolab/%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87-change-log-%E0%B8%87%E0%B9%88%E0%B8%B2%E0%B8%A2-%E0%B9%86-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-conventional-commit-e11324a8c65f)